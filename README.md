# General Information

## Maintainers
Stefan Ritt [stefan.ritt@psi.ch]
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Luca Galli [luca.galli@pi.infn.it]
Marco Francesconi [marco.francesconi@pi.infn.it]

# Origin
This repository was originally part of a larger repository that is now available here:
https://bitbucket.org/twavedaq/wavedaq_old.git

Note that the major part of the history was preserved when splitting the repository.

# Description
This repository contains files of the PCB designs for WaveDAQ.

There is a main repository containing all WaveDAQ repositories as submodules:
https://bitbucket.org/twavedaq/wavedaq_main.git
